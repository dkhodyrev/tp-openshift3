from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from admin import admin_site


urlpatterns = [
    url(r'^admin/', include(admin_site.urls)),
    url(r'^drawings/', include('drawings_show.urls')),
    url(r'^$', include('drawings_show.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login',
        {'template_name': 'drawings_show/login.html'}, name='login'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
