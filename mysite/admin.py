# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.admin import site

__author__ = 'alexey.nakoryakov'

admin_site = site
admin_site.site_header = 'Администрирование сайта ПБГС pbgs.ru'
admin_site.site_title = 'Админка ПБГС'
