#!/usr/bin/env python

from setuptools import setup

setup(
    name='Drawings',
    version='2.0',
    description='PGBS on OpenShift',
    author='Aleksey Nakoryakov',
    author_email='alexey.nakoryakov@glavstroy.ru',
    url='http://www.python.org/sigs/distutils-sig/',
)
