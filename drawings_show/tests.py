# -*- coding: utf8 -*-

from django.utils import unittest
from drawings_show.models import Drawing, Revision

def prepare_database():
    d1 = Drawing.objects.create(number=u"Тестовый шифр", page=1)
    d2 = Drawing.objects.create(number=u"Тестовый шифр", page=2)
    Revision.objects.create(drawing=d1, number=0, name=u'Тестовое наименование')
    Revision.objects.create(drawing=d1, number=1, name=u'Тестовое наименование')
    Revision.objects.create(drawing=d2, number=0, name=u'Тестовое наименование')

class DrawingTests(unittest.TestCase):
    def test_saving(self):
        ''' Tests if spaces in number of Drawing changes to '_' via save '''
        self.drawing = Drawing.objects.create(number=u"Тестовый шифр", page=1)
        self.assertEqual(self.drawing.number, u"Тестовый_шифр")

class IndexViewTests(unittest.TestCase):
    def setUp(self):
        prepare_database()

    def test_index_counting_revisions(self):
        pass