# -*- coding: utf8 -*-
from django.shortcuts import render_to_response, get_object_or_404, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from django.db.models import Count

from django.views.generic import ListView, UpdateView, DetailView, TemplateView, RedirectView
from django.utils.decorators import method_decorator

from models import Drawing, Revision, DrawingQuery
from models import normalize_number
from forms import FindDrawingForm
from datetime import date


class RevisionRedirectView(RedirectView):
    pass


class RevisionsView(ListView):
    model = Revision

    def get_queryset(self):
        return self.drawing.revision_set.all()

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        drawing_id = kwargs['drawing_id']
        self.drawing = Drawing.objects.get(pk=drawing_id)
        return super(RevisionsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RevisionsView, self).get_context_data(**kwargs)
        context['drawing'] = self.drawing
        return context


class SectionsStatisticsView(ListView):
    model = Drawing
    template_name = 'drawings_show/sections_stats.html'

    def __init__(self, *args, **kwargs):
        super(SectionsStatisticsView, self).__init__(*args, **kwargs)

    def get_queryset(self):
        qs = Drawing.objects.values('number').annotate(ncount=Count('number'))
        return qs

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(SectionsStatisticsView, self).dispatch(request, *args, **kwargs)


class RevisionChangeState(UpdateView):
    model = Revision


class IndexView(TemplateView):
    template_name = 'drawings_show/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['count'] = Revision.objects.count()
        return context


class RevisionDetail(DetailView):
    model = Revision
    template_name = 'drawings_show/show_drawing_status.html'

    def dispatch(self, request, *args, **kwargs):
        revision = self.get_object()
        drawing = revision.drawing
        DrawingQuery.objects.create(number=drawing.number, page=drawing.page, revision=revision.number)
        return super(RevisionDetail, self).dispatch(request, *args, **kwargs)


# =============================================================================
# Views
# =============================================================================
def show(request):
    req = request.GET or None
    number = req.get('number', '')
    page = req.get('page', '')
    rev = req.get('revision', '')

    DrawingQuery.objects.create(number=number, page=page, revision=rev)
    normalized_number = normalize_number(number)

    # TODO: Исправить это! Сейчас берётся непонятно какой чертеж
    drawing = Drawing.objects.filter(normalized_number=normalized_number,
                                     page=page)
    if drawing:
        drawing = drawing[0]
    else:
        return drawing_not_found(request)

    if drawing.revision_set.exclude(is_deleted=True).order_by('-number'):
        last_revision = drawing.revision_set.exclude(is_deleted=True).order_by('-number')[0]
        first_revision = drawing.revision_set.exclude(is_deleted=True).order_by('number')[0]
    else:
        return drawing_not_found(request)
    try:
        if first_revision.number == -1:
            revision = first_revision
        else:
            revision = drawing.revision_set.get(number__exact=rev, is_deleted=False)
    except Revision.DoesNotExist:
        return revision_not_found(request, drawing)

    return render_to_response('drawings_show/show_drawing_status.html', {
        'drawing': drawing,
        'revision': revision,
        'last_revision': last_revision,
    })

# @login_required
@csrf_exempt
def add_drawing(request):
    if request.POST:
        req = request.POST

        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = authenticate(username=username, password=password)
        if user and user.is_active:
            login(request, user)

            number = req['number']
            page = req['page']
            name = req['name']
            rev = req['revision']
            date = req['date']
            cover_letter_number = req.get('cover_letter_number', '')
            normalized_number = normalize_number(number)
            try:
                drawing = Drawing.objects.get(normalized_number=normalized_number, page=page)
            except Drawing.DoesNotExist:
                drawing = Drawing.objects.create(number=number, page=page)
            if not drawing.revision_set.filter(number__exact=rev, is_deleted__exact=False).exists():
                Revision.objects.create(drawing=drawing,
                                        number=rev,
                                        date=date,
                                        name=name,
                                        cover_letter_number=cover_letter_number)
                message = "Чертеж добавлен успешно"
            else:
                message = "Ошибка! Данный чертеж уже есть в базе!"
        else:
            message = "Ошибка! Ошибка авторизации!"
    else:
        message = "Error! Request didn't found"
    return HttpResponse(message)


@login_required
def find_drawing(request):
    find_form = FindDrawingForm(request.GET or None)
    if request.GET:
        if find_form.is_valid():
            drawing = get_object_or_404(Drawing, number__exact=request.GET["number"], page__exact=request.GET["page"])
            return HttpResponseRedirect(reverse('show_drawing', args=(drawing.pk,)))

    return render(
        request,
        'drawings_show/find_drawing.html', {
            'find_form': find_form,
        })


@login_required
def edit_revision(request, revision_id):
    revision = get_object_or_404(Revision, pk=revision_id)
    drawing = revision.drawing

    if request.GET:
        drawing_id = request.GET.get("drawing_id", None)

    elif request.POST:
        drawing_id = request.POST.get("drawing_id", None)
        if "delete" in request.POST:
            revision.is_deleted = True
        elif "restore" in request.POST:
            revision.is_deleted = False
        revision.save()
        return HttpResponseRedirect(reverse("show_drawing", args=(drawing.pk,)))

    return render(
        request,
        'drawings_show/edit_revision.html', {
            'drawing_id': drawing_id,
            'revision': revision,
        })


@login_required
def get_report(request):
    from excel_manipulations import create_drawings_report
    revisions = Revision.objects.filter(is_deleted__exact=False)
    rep = create_drawings_report(revisions)
    response = HttpResponse(rep, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    filename = "report %s.xlsx" % date.today()
    response['Content-Disposition'] = 'attachment; filename="%s"' % filename
    return response


def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse("drawings.drawings_show.views.index"))


def drawing_not_found(request):
    """ Обработка отсутствующего документа """
    req = request.GET or None
    number = req.get('number', '')
    page = req.get('page', '')
    rev = req.get('revision', '')
    if not number:
        error_message = u'Не указан шифр чертежа!'
    elif not page:
        error_message = u'Не указан номер листа!'
    elif not rev:
        error_message = u'Не указан номер изменения!'
    else:
        error_message = u"Ошибка! Не найден чертеж: %s, лист %s" % (number, page)
    return HttpResponse(error_message)


def revision_not_found(request, drawing):
    """Обработка отсутствующей ревизии"""
    req = request.GET or None

    rev = req.get('revision', '')
    if not rev:
        error_message = u'Не указан номер изменения!'
    else:
        error_message = u"Ошибка! Не найдено изменение %s чертежа: %s, лист %s" % (rev, drawing.number, drawing.page)
        error_message += u"<br/>В базе сохранены данные о следующих изм'ах"
        for r in drawing.revision_set.order_by('number'):
            error_message += u"<br/>%s" % unicode(r)

    return HttpResponse(error_message)


def handler404(request):
    req = request.GET or None
    number = req.get('number', '')
    name = req.get('name', '')
    page = req.get('page', '')
    rev = req.get('revision', '')
    d = u"%s, лист %i. %s" % (number, page, name)
    r = u". изм.%i " % rev if rev != 0 else ""
    drawing = u"%s%s" % (d, r)
    return render_to_response('404.html', {
        'drawing': drawing,
    })


def handler500(request):
    pass
