# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

__author__ = 'alexey.nakoryakov'


def create_titles(ws):
    ws['A1'] = '№ п/п'
    ws['B1'] = 'Лист'
    ws['C1'] = 'Шифр'
    ws['D1'] = 'Наименование'
    ws['E1'] = 'Изм'
    ws['F1'] = 'Дата'
    ws['G1'] = 'Номер сопроводительного'


def create_drawings_report(revisions):
    wb = Workbook()
    ws = wb.active
    create_titles(ws)
    index = 2
    for rev in revisions:
        ws.cell(row=index, column=1).value = index - 1
        ws.cell(row=index, column=2).value = rev.drawing.page
        ws.cell(row=index, column=3).value = rev.drawing.number
        ws.cell(row=index, column=4).value = rev.name
        ws.cell(row=index, column=5).value = rev.number
        ws.cell(row=index, column=6).value = rev.date
        ws.cell(row=index, column=7).value = rev.cover_letter_number
        index += 1
    return save_virtual_workbook(wb)
