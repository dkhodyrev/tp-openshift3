from django.conf.urls import url

import views
from views import RevisionsView, SectionsStatisticsView


urlpatterns = (
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^show/$', views.show, name='show'),
    url(r'^add/$', views.add_drawing, name='add_drawing'),
    url(r'^logout/$', views.user_logout, name='user_logout'),
    url(r'^find/$', views.find_drawing, name='find_drawing'),
    url(r'^drawing/(?P<drawing_id>\d+)/$', RevisionsView.as_view(), name='show_drawing'),
    url(r'^revision/(?P<revision_id>\d+)/$', views.edit_revision, name='edit_revision'),
    url(r'^stats/$', SectionsStatisticsView.as_view(), name='show_stats'),
    url(r'^report/$', views.get_report, name='get_report'),
)
