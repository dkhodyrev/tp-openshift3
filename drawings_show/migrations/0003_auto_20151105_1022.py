# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('drawings_show', '0002_revision_cover_letter_number'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='drawing',
            options={'verbose_name': '\u0447\u0435\u0440\u0442\u0435\u0436', 'verbose_name_plural': '\u0447\u0435\u0440\u0442\u0435\u0436\u0438'},
        ),
        migrations.AlterModelOptions(
            name='revision',
            options={'verbose_name': '\u0440\u0435\u0432\u0438\u0437\u0438\u044f \u0447\u0435\u0440\u0442\u0435\u0436\u0430', 'verbose_name_plural': '\u0440\u0435\u0432\u0438\u0437\u0438\u0438 \u0447\u0435\u0440\u0442\u0435\u0436\u0435\u0439'},
        ),
        migrations.AlterField(
            model_name='revision',
            name='date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='\u041e\u0442'),
        ),
    ]
