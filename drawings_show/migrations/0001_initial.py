# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Drawing',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=200, verbose_name='\u0428\u0438\u0444\u0440 \u043f\u0440\u043e\u0435\u043a\u0442\u0430')),
                ('page', models.CharField(max_length=20, verbose_name='\u041b\u0438\u0441\u0442')),
            ],
        ),
        migrations.CreateModel(
            name='DrawingQuery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=200, verbose_name='\u0428\u0438\u0444\u0440 \u043f\u0440\u043e\u0435\u043a\u0442\u0430')),
                ('page', models.CharField(max_length=20, verbose_name='\u041b\u0438\u0441\u0442')),
                ('revision', models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0438\u0437\u043c\u0430')),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='\u0412\u0440\u0435\u043c\u044f \u0437\u0430\u043f\u0440\u043e\u0441\u0430')),
            ],
        ),
        migrations.CreateModel(
            name='Revision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1000, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435 \u0447\u0435\u0440\u0442\u0435\u0436\u0430')),
                ('number', models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0438\u0437\u043c\u0430')),
                ('date', models.DateField(default=datetime.datetime.now, verbose_name='\u041e\u0442')),
                ('is_deleted', models.BooleanField(default=False, verbose_name='\u0423\u0434\u0430\u043b\u0435\u043d')),
                ('drawing', models.ForeignKey(to='drawings_show.Drawing')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='drawing',
            unique_together=set([('number', 'page')]),
        ),
    ]
