# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drawings_show', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='revision',
            name='cover_letter_number',
            field=models.CharField(max_length=100, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0441\u043e\u043f\u0440\u043e\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0433\u043e', blank=True),
        ),
    ]
