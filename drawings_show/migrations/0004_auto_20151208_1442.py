# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('drawings_show', '0003_auto_20151105_1022'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='drawingquery',
            options={'verbose_name': '\u0437\u0430\u043f\u0440\u043e\u0441 \u0447\u0435\u0440\u0442\u0435\u0436\u0430', 'verbose_name_plural': '\u0437\u0430\u043f\u0440\u043e\u0441\u044b \u0447\u0435\u0440\u0442\u0435\u0436\u0435\u0439'},
        ),
        migrations.AlterField(
            model_name='revision',
            name='drawing',
            field=models.ForeignKey(verbose_name='\u0427\u0435\u0440\u0442\u0435\u0436', to='drawings_show.Drawing'),
        ),
    ]
