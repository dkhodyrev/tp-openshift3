# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from drawings_show.models import normalize_number
from operator import attrgetter
from collections import defaultdict


def normalize_numbers_and_union(apps, schema_editor):
    Drawing = apps.get_model('drawings_show', 'Drawing')
    Revision = apps.get_model('drawings_show', 'Revision')
    for dr in Drawing.objects.all():
        dr.normalized_number = normalize_number(dr.number)
        dr.save()
    drawings = (Drawing.objects.all()
                .values('normalized_number')
                .annotate(count=models.Count('number'))
                .filter(count__gt=1))
    deleted_drawings_count = 0
    # deleted_revisions_count = 0
    for ndr in drawings:
        drs = Drawing.objects.filter(normalized_number=ndr['normalized_number'])
        revisions = Revision.objects.filter(drawing__in=map(attrgetter('id'), drs))
        new_dr = revisions.latest('date').drawing
        revisions = list(revisions)
        for rev in revisions:
            rev.drawing = new_dr
            rev.save()
        deleted_drawings_count += drs.exclude(id=new_dr.id).count()
        drs.exclude(id=new_dr.id).delete()
        # d = defaultdict(list)
        # for rev in revisions:
        #     d[rev.number].append(rev)
        # for k, v in d.items():
        #     if len(v) > 1:
        #         for r in sorted(v, key=attrgetter('date'))[:-1]:
        #             r.delete()
        #             deleted_revisions_count += 1
    print('!!!!!!!!Deleted Drawings Count = %d' % deleted_drawings_count)
    # print('!!!!!!!!Deleted Revisions Count = %d' % deleted_revisions_count)


class Migration(migrations.Migration):

    dependencies = [
        ('drawings_show', '0004_auto_20151208_1442'),
    ]

    operations = [
        migrations.AddField(
            model_name='drawing',
            name='normalized_number',
            field=models.CharField(verbose_name='\u041d\u043e\u0440\u043c\u0430\u043b\u0438\u0437\u043e\u0432\u0430\u043d\u043d\u044b\u0439 \u0448\u0438\u0444\u0440 \u043f\u0440\u043e\u0435\u043a\u0442\u0430', max_length=200, editable=False, blank=True),
        ),
        # migrations.RunPython(normalize_numbers_and_union),
    ]
