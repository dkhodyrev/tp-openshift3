# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib import admin
from . import models


@admin.register(models.Drawing)
class DrawingAdmin(admin.ModelAdmin):
    list_display = ('number', 'page', )
    search_fields = ('number', )
    readonly_fields = ('normalized_number', )


@admin.register(models.Revision)
class RevisionAdmin(admin.ModelAdmin):
    list_display = ('drawing', 'name', 'number', 'date', 'cover_letter_number', 'is_deleted')
    search_fields = ('drawing__number', )
    date_hierarchy = 'date'


@admin.register(models.DrawingQuery)
class DrawingQueryAdmin(admin.ModelAdmin):
    list_display = ('number', 'page', 'revision', 'timestamp', )
    date_hierarchy = 'timestamp'
