# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import AppConfig

__author__ = "Aleksey Nakoryakov"


class DrawingsShowConfig(AppConfig):
    name = 'drawings_show'
    verbose_name = 'Чертежи'
