# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.timezone import now


def normalize_number(number):
    return number.replace(' ', '_').replace('.', '_').replace('-', '_')


class Drawing(models.Model):
    number = models.CharField(max_length=200, verbose_name=u'Шифр проекта')
    page = models.CharField(max_length=20, verbose_name=u'Лист')
    normalized_number = models.CharField(max_length=200,
                                         blank=True,
                                         editable=False,
                                         verbose_name='Нормализованный шифр проекта')

    def save(self, *args, **kwargs):
        self.number = self.number.replace(" ", "_")
        self.normalized_number = normalize_number(self.number)
        super(Drawing, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('show_drawing', kwargs={'drawing_id': self.pk})

    def __unicode__(self):
        return u"%s, лист %s." % (self.number, self.page)

    class Meta:
        unique_together = ('number', 'page')
        verbose_name = 'чертеж'
        verbose_name_plural = 'чертежи'


class Revision(models.Model):
    drawing = models.ForeignKey(Drawing, verbose_name='Чертеж')
    name = models.CharField(max_length=1000, verbose_name=u'Наименование чертежа')
    number = models.IntegerField(verbose_name=u'Номер изма')
    date = models.DateField(default=now, verbose_name=u'От')
    is_deleted = models.BooleanField(default=False, verbose_name=u'Удален')
    cover_letter_number = models.CharField(max_length=100,
                                           blank=True,
                                           verbose_name=u'Номер сопроводительного')

    def __unicode__(self):
        if self.number == -1:
            rev = "Аннулировано"
        else:
            rev = u". изм.%i " % self.number if self.number != 0 else ""
        return u"%s %s%s от %s" % (self.drawing, self.name, rev, self.date)

    class Meta:
        verbose_name = 'ревизия чертежа'
        verbose_name_plural = 'ревизии чертежей'


class DrawingQuery(models.Model):
    number = models.CharField(max_length=200, verbose_name=u'Шифр проекта')
    page = models.CharField(max_length=20, verbose_name=u'Лист')
    revision = models.IntegerField(verbose_name=u'Номер изма')
    timestamp = models.DateTimeField(verbose_name=u'Время запроса', auto_now_add=True)

    class Meta:
        verbose_name = 'запрос чертежа'
        verbose_name_plural = 'запросы чертежей'
