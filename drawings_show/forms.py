# -*- coding: utf8 -*-
from django import forms


class FindDrawingForm(forms.Form):
    number = forms.CharField(required=True, label=u'Шифр')
    page = forms.IntegerField(required=True, label=u'Лист')

    def __init__(self, *arg, **kwarg):
        super(FindDrawingForm, self).__init__(*arg, **kwarg)
        self.empty_permitted = False