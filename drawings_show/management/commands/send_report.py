# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from operator import itemgetter
from django.core.management.base import BaseCommand
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.utils import timezone
from django.conf import settings

from drawings_show.excel_manipulations import create_drawings_report
from drawings_show.models import Revision

__author__ = "Aleksey Nakoryakov"


class Command(BaseCommand):
    def handle(self, *args, **options):
        revisions = Revision.objects.filter(is_deleted__exact=False)
        report = create_drawings_report(revisions)
        subject = u'Журнал регистрации чертежей'
        to = map(itemgetter(0),  settings.ADMINS[0])
        # response = HttpResponse(report, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        # filename = "report %s.xlsx" % date.today()
        # response['Content-Disposition'] = 'attachment; filename="%s"' % filename
